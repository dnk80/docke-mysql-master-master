# docker-mysql-master-master

This article consolidates information from several sources into the format I use to setup MySQL Master/Master Replication. 

In this docker-compose example I using next values:
DB nam = db
username = admin
password = admin
IP mysql master1 10.5.0.2
IP mysql master1 10.5.0.3

Sugar & Step-by-Step

Edit /etc/my.cnf on both servers
Add the following information to the bottom of the [mysqld] section

Server A

[root@mysqla ~]# vi /etc/my.cnf
	
    server-id=1
    log-bin="mysql-bin"
    binlog-do-db=name_of_database
    replicate-do-db=name_of_database
    relay-log="mysql-relay-log"
    auto-increment-offset = 1
    
Server B

[root@mysqlb ~]# vi /etc/my.cnf
    
    server-id=2
    log-bin="mysql-bin"
    binlog-do-db=name_of_database
    replicate-do-db=name_of_database
    relay-log="mysql-relay-log"
    auto-increment-offset = 2
    
Make sure you replace name_of_database with the name of the database that you want to replicate

Restart and enable the MySQL daemon on each server
Server A

[root@mysqla ~]# systemctl restart mysqld

[root@mysqla ~]# systemctl enable mysqld


Server B

[root@mysqlb ~]# systemctl restart mysqld

[root@mysqlb ~]# systemctl enable mysqld


Create the replicator user on each server

[root@mysqla ~]# mysql -u root -p
 
mysql> CREATE USER 'replicator'@'%' IDENTIFIED BY 'change_me';

mysql> GRANT REPLICATION SLAVE ON foo.* TO 'replicator'@'%'

[root@mysqlb ~]# mysql -u root -p
 
mysql> CREATE USER 'replicator'@'%' IDENTIFIED BY 'change_me';

mysql> GRANT REPLICATION SLAVE ON foo.* TO 'replicator'@'%'

Get log file information for use on the other server

Server A

[root@mysqla ~]# mysql -u root -p
 
mysql> SHOW MASTER STATUS;
 
+------------------+----------+------------------+------------------+
| File             | Position | Binlog_Do_DB     | Binlog_Ignore_DB |
+------------------+----------+------------------+------------------+
| mysql-bin.000001 | 154      | name_of_database |                  |
+------------------+----------+------------------+------------------+
1 row in set (0.00 sec)
Note the "File" and "Position" from this command

Server B

[root@mysqlb ~]# mysql -u root -p
 
mysql> STOP SLAVE;
 
mysql> CHANGE MASTER TO MASTER_HOST = 'Server A IP Address or HOSTNAME',MASTER_USER = 'replicator', MASTER_PASSWORD = 'change_me', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = 154;

mysql> START SLAVE;

Repeat the same steps on Server B

Server B

[root@mysqlb ~]# mysql -u root -p mysql> SHOW MASTER STATUS;
 
+------------------+----------+------------------+------------------+
| File             | Position | Binlog_Do_DB     | Binlog_Ignore_DB |
+------------------+----------+------------------+------------------+
| mysql-bin.000001 | 154      | name_of_database |                  |
+------------------+----------+------------------+------------------+
1 row in set (0.00 sec)
Note the "File" and "Position" from this command

Server A

[root@mysqla ~]# mysql -u root -p 
 
mysql> STOP SLAVE; CHANGE MASTER TO MASTER_HOST = 'Server B IP Address or HOSTNAME', MASTER_USER = 'replicator', MASTER_PASSWORD = 'passw0rd', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = 154; 

mysql> START SLAVE;
